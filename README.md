# README #
This is a repo with T-Systems Java School preliminary examination task. The solutions are wriitten using Java 1.8.

The exam includes 3 tasks to be done:  [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author : MD AL MAMUNUR RASHID
* Codeship status: ![Codeship Status for tschool/javaschoolexam](https://app.codeship.com/projects/4887ecc0-103e-0137-0a5a-463f023d5fd7/status?branch=master)
